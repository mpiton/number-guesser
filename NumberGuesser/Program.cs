﻿using System;

// Namespace
namespace NumberGuesser
{
    //Class
    class Program
    {
        
        //Entry Point Méthod
        static void Main(string[] args)
        {
            //Show App Infos
            getAppInfo();
            //Ask Users Name
            GreetUser();
            //Start the game
            while (true)
            {

                //Set correct number (create a new random object)
                Random random = new Random();
                int correctNumber = random.Next(1, 10);


                // Init guess var
                int guess = 0;

                Console.WriteLine("Guess a number beetween 1 and 10");

                //while guess is not correct
                while (guess != correctNumber)
                {
                    string input = Console.ReadLine();
                    //Make sure it's a number
                    if (!int.TryParse(input, out guess))
                    {
                        PrintColorMessage(ConsoleColor.Yellow, "Please enter a correct number !");
                        continue;
                    }
                    // Cast to int and put in guess
                    guess = Int32.Parse(input);
                    //Match to the correct number
                    if (guess != correctNumber)
                    {
                        PrintColorMessage(ConsoleColor.Red, "Bad answer !");
                    }
                    else
                    {
                        PrintColorMessage(ConsoleColor.Green, "You win !");


                        // Ask play again
                        Console.WriteLine("Play Again ? [Y or N]");
                        //Get answer
                        string answer = Console.ReadLine().ToUpper();
                        if(answer == "Y")
                        {
                            continue;
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
        }
        static void getAppInfo()
        {
            //Set app vars
            string appName = "Number Guesser";
            string version = "1.0.0";
            string appAuthor = "Mathieu Piton";

            //Change text color
            Console.ForegroundColor = ConsoleColor.Green;
            //Write out app info
            Console.WriteLine("{0}: Version {1} by {2}", appName, version, appAuthor);
            //reset text color
            Console.ResetColor();
        }
        static void GreetUser()
        {
            Console.WriteLine("What's your name ?");

            //Get user input
            string inputName = Console.ReadLine();

            Console.WriteLine("Hello {0}, let's play a game..", inputName);
        }
        //print color message
        static void PrintColorMessage(ConsoleColor color, string message)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }
    }
}
